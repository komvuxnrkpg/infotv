INSTDIR=/opt/infotv
BINDIR=/usr/local/bin
SYSTEMD=/usr/lib/systemd/system
DEBDIR=deb-package
PKGINSTDIR=/usr/share/infotv
PKGBINDIR=/usr/bin
DEBINST=$(DEBDIR)$(PKGINSTDIR)
DEBBIN=$(DEBDIR)$(PKGBINDIR)
DEBSYSD=$(DEBDIR)$(SYSTEMD)

.PHONY: install install-files uninstall disable help prereqs package deb prepare-deb clean clean-deb

help:
	@echo "Run \"make prereqs\" to make sure required prerequisites are \
	installed, then run \"make install\"".

install: install-files
	@echo "Info-TV installed. Please update /boot/infotv.txt and \
	/boot/zerotier.txt with the correct information.\n\n\
	See README.md for help.\n\n\
	You\'ll also need to run infotv-setup."

disable:
	infotv-config disable

uninstall: uninstall-files
	@echo "Info-TV disabled and uninstalled."

uninstall-files: disable
	sudo unlink $(SYSTEMD)/config-infotv.service
	sudo unlink $(SYSTEMD)/config-zerotier.service
	sudo unlink $(BINDIR)/infotv-setup
	sudo unlink $(BINDIR)/infotv-config
	sudo unlink $(BINDIR)/infotv-config-zerotier
	sudo rm -r $(INSTDIR)

install-files:
	sudo install -D --target-directory $(INSTDIR) src/infotv-launch.sh
	sudo install -D --target-directory $(INSTDIR) src/infotv-setup.sh
	sudo install -D --target-directory $(INSTDIR) src/config-infotv.sh
	sudo install -D --target-directory $(INSTDIR) src/config-zerotier.sh
	sudo install -D --target-directory $(INSTDIR) src/bash_profile.sh
	sudo install -D --target-directory $(INSTDIR) src/generate-ssh-host-keys.sh
	sudo install -D --mode=644 --target-directory $(INSTDIR) src/infotv-launch.desktop
	sudo install -D --mode=644 --target-directory $(INSTDIR) src/config-infotv.service
	sudo install -D --mode=644 --target-directory $(INSTDIR) src/config-zerotier.service
	sudo install -D --mode=644 --target-directory $(INSTDIR) src/reboot-daily.service
	sudo install -D --mode=644 --target-directory $(INSTDIR) src/reboot-daily.timer
	sudo install -D --mode=644 --target-directory $(INSTDIR) src/generate-ssh-host-keys.service
	sudo ln -s $(INSTDIR)/config-infotv.service $(SYSTEMD)/
	sudo ln -s $(INSTDIR)/config-zerotier.service $(SYSTEMD)/
	sudo ln -s $(INSTDIR)/reboot-daily.service $(DEBSYSD)/
	sudo ln -s $(INSTDIR)/reboot-daily.timer $(DEBSYSD)/
	sudo ln -s $(INSTDIR)/generate-ssh-host-keys.service $(DEBSYSD)/
	sudo install -D --mode=644 src/i3-config $(INSTDIR)/config/i3/config
	sudo ln -s $(INSTDIR)/infotv-launch.sh $(BINDIR)/infotv-launch
	sudo ln -s $(INSTDIR)/infotv-setup.sh $(BINDIR)/infotv-setup
	sudo ln -s $(INSTDIR)/config-infotv.sh $(BINDIR)/infotv-config
	sudo ln -s $(INSTDIR)/config-zerotier.sh $(BINDIR)/infotv-config-zerotier
	sudo ln -s $(INSTDIR)/generate-ssh-host-keys.sh $(BINDIR)/generate-ssh-host-keys

prereqs:
	@echo -e "Installing minimal prerequisites:"
	sudo apt install xserver-xorg-core xserver-xorg-video-fbturbo i3 dex \
	xserver-xorg-input-libinput x11-xkb-utils unclutter fonts-dejavu \
	chromium-browser
	sudo apt install --no-install-recommends xinit

package: deb

deb: prepare-deb
	dpkg-deb -b $(DEBDIR) .

prepare-deb: clean-deb $(DEBINST) $(DEBBIN) $(DEBSYSD)
	sudo install -D --target-directory $(DEBINST) src/infotv-launch.sh
	sudo install -D --target-directory $(DEBINST) src/infotv-setup.sh
	sudo install -D --target-directory $(DEBINST) src/config-infotv.sh
	sudo install -D --target-directory $(DEBINST) src/config-zerotier.sh
	sudo install -D --target-directory $(DEBINST) src/bash_profile.sh
	sudo install -D --target-directory $(DEBINST) src/generate-ssh-host-keys.sh
	sudo install -D --mode=644 --target-directory $(DEBINST) src/infotv-launch.desktop
	sudo install -D --mode=644 --target-directory $(DEBINST) src/config-infotv.service
	sudo install -D --mode=644 --target-directory $(DEBINST) src/config-zerotier.service
	sudo install -D --mode=644 --target-directory $(DEBINST) src/reboot-daily.service
	sudo install -D --mode=644 --target-directory $(DEBINST) src/reboot-daily.timer
	sudo install -D --mode=644 --target-directory $(DEBINST) src/generate-ssh-host-keys.service
	sudo ln -s $(PKGINSTDIR)/config-infotv.service $(DEBSYSD)/
	sudo ln -s $(PKGINSTDIR)/config-zerotier.service $(DEBSYSD)/
	sudo ln -s $(PKGINSTDIR)/reboot-daily.service $(DEBSYSD)/
	sudo ln -s $(PKGINSTDIR)/reboot-daily.timer $(DEBSYSD)/
	sudo ln -s $(PKGINSTDIR)/generate-ssh-host-keys.service $(DEBSYSD)/
	sudo install -D --mode=644 src/i3-config $(DEBINST)/config/i3/config
	sudo ln -s $(PKGINSTDIR)/infotv-launch.sh $(DEBBIN)/infotv-launch
	sudo ln -s $(PKGINSTDIR)/infotv-setup.sh $(DEBBIN)/infotv-setup
	sudo ln -s $(PKGINSTDIR)/config-infotv.sh $(DEBBIN)/infotv-config
	sudo ln -s $(PKGINSTDIR)/config-zerotier.sh $(DEBBIN)/infotv-config-zerotier
	sudo ln -s $(PKGINSTDIR)/generate-ssh-host-keys.sh $(DEBBIN)/generate-ssh-host-keys
	grep -e '^Version:' $(DEBDIR)/DEBIAN/control | sudo tee $(DEBINST)/infotv-version.txt

$(DEBDIR)/%:
	mkdir -p $@

clean-deb:
	sudo rm -rf $(DEBDIR)/usr

clean: clean-deb
