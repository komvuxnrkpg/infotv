# infotv
A (fairly?) simple way to set up information screens on Raspberry Pi.

Install a minimal Linux on your Raspberry Pi, and make sure it uses systemd
(most do) rather than sysvinit, upstart, or something else.

Make sure i3, Dex, Unclutter, and either Midori or the Chromium browser are
installed.

If you'd like to use ZeroTier for virtual LAN, run
'''sudo ./add-zerotier-repository.sh''' to make it available, then
'''sudo apt-get update''' and '''sudo apt-get install zerotier-one'''.

Run '''make install'''.

## deb package
Run '''make deb''' to create a package (that assumes a fairly clean Raspberry
Pi OS Lite).

Install the package with '''sudo dpkg -i [filename]'''.

Install dependencies (i3, chromium, et. al.) with '''sudo apt -f install'''

## /boot/infotv.txt
A simple config file with two fields: url and name
"name" sets the local hostname
"url" sets what url to display the contents of

The URL will be stored in /etc/infotv.conf for later retreival.

Example:
name=infotv-005
url=https://phoronix.com

If either key does not exist, said setting will not change.

This file will be removed after applying the config.

## /boot/zerotier.txt
A list of zerotier networks (network IDs) to connect to, any whitespace
between them.
If this file exists, the ZeroTier settings will be reset on the next startup,
any current networks will be forgotten and new networks (if any) will be joined
in their stead, make sure you save your list of networks in order to join the
same ones with a new node ID.

if the network isn't open to the public, you need to log in and approve the
request to join.

This file will be removed after resetting ZeroTier, starting it, and joining
the listed networks.
