#!/usr/bin/bash

#Generate SSH host keys if they don't exist

if [ 0 -ne $(id -u) ]; then
  echo "Need ro run setup as root."
  echo "Running sudo..."
  sudo "$0"
  exit
fi

pushd /etc/ssh > /dev/null
configkeys=($(grep -e "^[^#]*HostKey" /etc/ssh/sshd_config |sed -e 's/.*HostKey[[:space:]]\+//g'))
keycount=${#configkeys[@]}
haskey=0
for k in ${configkeys[@]}; do
  if [ -e $k ]; then
    haskey=1
    break
  fi
done
if [ $haskey -eq 0 ]; then
  if [ $keycount -gt 0 ]; then
    for t in ed25519 ecdsa rsa; do
      for k in ${configkeys[@]}; do
        if [ "$k" ~= /.*$t.*/ ]; then
          ssh-keygen -t $t -f $k -q -N ""
          break 2
        fi
      done
    done
  elif [ $(ls /etc/ssh/ssh_host_*_key | wc -l) -eq 0 ]; then
    ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -q -N ""
  fi
fi
popd > /dev/null
