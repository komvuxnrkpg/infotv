#!/usr/bin/bash

# Checks /boot/infotv.txt for fields "name" and "url"
# If "name" is set, set local hostname to that
# If "url" is set, make sure the kiosk displays said URL (or local path)

if [ 0 -ne $(id -u) ]; then
  echo "Need ro run setup as root."
  echo "Running sudo..."
  sudo "$0"
  exit
fi

if [ -f "/boot/infotv.txt" ]; then
  #echo "[it] $(date +'%Y-%m-%d %H:%M:%S') /boot/infotv.txt exists, changing config." >> /var/log/infotv.log

  # Check the wanted hostname, filtering to simple/valid characters
  wantedname=$(cat /boot/infotv.txt | grep "name=" | sed -e 's/^[^=]*=//' -e 's/\r//g')
  currentname=$(hostname)
  if [ -n "${wantedname}" ]; then
    # Set host name
    if hostname ${wantedname} > /dev/null ; then
      sed -e "s/${currentname}/${wantedname}/g" /etc/hosts > /tmp/hosts
      echo "${wantedname}" > /etc/hostname
      mv /tmp/hosts /etc/hosts
      #echo "[it] $(date +'%Y-%m-%d %H:%M:%S') Changed hostname to ${wantedname}." >> /var/log/infotv.log
    fi
  fi

  # Check group, filtering for simple filenames
  wantedurl="$(cat /boot/infotv.txt | grep 'url=' | sed -e 's/\r//g')"

  # Save group to config file, which will be read by the start script.
  if [ -n "${wantedurl}" ]; then
    echo "${wantedurl}" > /etc/infotv.conf
    #echo "[it] $(date +'%Y-%m-%d %H:%M:%S') Changed URL to ${wantedurl}." >> /var/log/infotv.log
  fi

  # Remove /boot/infotv.txt in order to not run this config again (unless said file is recreated).
  #echo "[it] $(date +'%Y-%m-%d %H:%M:%S') Removing /boot/infotv.txt." >> /var/log/infotv.log
  rm /boot/infotv.txt

fi

echo "=Hostname:" > /boot/infotv.current.txt
echo $(hostname) >> /boot/infotv.current.txt
if [ -e /etc/infotv.conf ]; then
  echo "=URL:" >> /boot/infotv.current.txt
  cat /etc/infotv.conf | sed -e 's/^url=//' >> /boot/infotv.current.txt
fi

