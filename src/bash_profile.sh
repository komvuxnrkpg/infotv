#!/usr/bin/env bash

. $HOME/.profile

if [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
  startx
fi
