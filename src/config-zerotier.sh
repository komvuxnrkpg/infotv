#!/usr/bin/bash

# Check that /boot/zerotier.txt exists, and if so:
# Remove any current networks and joins networks listed in /boot/zerotier.txt

if [ 0 -ne $(id -u) ]; then
  echo "Need ro run setup as root."
  echo "Running sudo..."
  sudo "$0"
  exit
fi

if [ -f /boot/zerotier.txt ]; then

  for n in $(cat /boot/zerotier.txt); do
    if [ "NEWID" = "$n" ]; then
      rm /var/lib/zerotier-one/identity.*
      sed -ie "s/\bNEWID\b//g" /boot/zerotier.txt
      break
    fi
  done

  if [ $(cat /boot/zerotier.txt | sed -e 's/\r//g' | wc -w) -gt 0 ]; then
    rm -rf /var/lib/zerotier-one/networks.d/*
    mkdir -p /var/lib/zerotier-one/networks.d
    for n in $(cat /boot/zerotier.txt); do
      touch /var/lib/zerotier-one/networks.d/$n.conf
    done
  fi

  rm /boot/zerotier.txt
fi

rm -f /boot/zerotier.current.txt

if [ -e /var/lib/zerotier-one/identity.public ]; then
  echo "=ID:" >> /boot/zerotier.current.txt
  echo $(cat /var/lib/zerotier-one/identity.public | sed -e 's/:.*//') >> /boot/zerotier.current.txt
fi

if [ -d /var/lib/zerotier-one/networks.d ]; then
  echo "=Networks:" >> /boot/zerotier.current.txt
  ls /var/lib/zerotier-one/networks.d/ | sed -e 's/\..*//g' | sort --unique >> /boot/zerotier.current.txt
fi
