#!/usr/bin/sh
if [ ! -e /etc/infotv.conf ]; then
  exit
fi
stream=$(grep "url=" /etc/infotv.conf | cut -c "5-")
if [ "_$(echo $stream|cut -c1-1)_" = "_/_" ]; then
  stream="file://$stream"
fi
for appl in chromium-browser chromium google-chrome google-chrome-stable google-chrome-beta google-chrome-dev; do
  if which $appl > /dev/null ; then
    break
  fi
  appl=""
done
export DISPLAY=:0.0
if [ -n "$appl" ]; then
  rm -r ~/.config/$(echo $appl | sed -e 's/-\(browser\|stable\|beta\|dev\)$//') 2>/dev/null
  rm -r ~/.cache/$(echo $appl | sed -e 's/-\(browser\|stable\|beta\|dev\)$//') 2>/dev/null
  $appl --kiosk --noerrdialogs --incognito ${stream}
elif which midori > /dev/null ; then
  midori $stream & sleep 60; midori -e fullscreen
else
  echo "Neither Chromium, Chrome, nor Midori found."
  echo "Trying to run default browser, probably not using kiosk mode."
  xdg-open "$stream"
fi
