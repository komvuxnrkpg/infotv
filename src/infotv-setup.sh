#!/usr/bin/bash

USER=$(who am i|awk '{print $1}')
GROUP=$(id -gn $USER)
HOMEDIR=$(getent passwd $USER | cut -d: -f6)

_status () {
  if [ -h $HOMEDIR/.config/autostart/infotv.desktop ]; then
    echo "Info-TV is in autostart for $USER."
  else
    echo "Info-TV is not in autostart for $USER."
  fi
  echo "Hostname: $(hostname)"
  if [ -f /etc/infotv.conf ]; then
    url=$(grep "url=" /etc/infotv.conf | sed -e 's/^[^=]\+=//')
    echo "URL: $url"
  else
    echo "WARNING: /etc/infotv.conf does not exist. URL is unknown."
  fi
  if [ -f /boot/infotv.txt ]; then
    echo "URL and/or hostname will be set at next boot."
    echo "Contents of /boot/infotv.txt:"
    echo "==="
    cat /boot/infotv.txt
    echo "==="
  fi
  if [ -f /boot/zerotier.txt ]; then
    echo "ZeroTier will be configured at next boot."
    echo "Contents of /boot/zerotier.txt:"
    echo "==="
    cat /boot/zerotier.txt
    echo "==="
  fi
}

_enable () {
  sudo systemctl enable config-infotv.service
  sudo systemctl enable config-zerotier.service
  sudo systemctl enable reboot-daily.timer
  sudo systemctl enable generate-ssh-host-keys.service

  if [ $(id -u $USER) -ne $(id -u) ]; then
    echo " :: Do you want to enable infotv for user \"$USER\"? [Y/n]"
    read okvar
    okvar=${okvar//[[:space:]]/}
    okvar=${okvar::1}
    okvar=${okvar,,}
    if [ "n" == "$okvar" ]; then
      echo "Log in as the user you want to set up for, and run $0"
      echo "Do not use sudo/su, the actual user will be detected."
      exit 0
    fi
  fi

  mydir=$(dirname "$(realpath "$0")")
  sudo -u $USER mkdir -p $HOMEDIR/.config/autostart
  sudo -u $USER ln -s "$mydir/infotv-launch.desktop" $HOMEDIR/.config/autostart/infotv-launch.desktop
  sudo -u $USER install -D --backup=numbered $mydir/config/i3/config $HOMEDIR/.config/i3/config
  sudo -u $USER install --backup=numbered $mydir/bash_profile.sh $HOMEDIR/.bash_profile
}

_disable () {
  sudo systemctl disable config-infotv.service
  sudo systemctl disable config-zerotier.service
  sudo systemctl disable reboot-daily.timer
  sudo systemctl disable generate-ssh-host-keys.service

  echo "You might want to restore the backups of -or remove- i3's config"\
       "file at ~/.config/i3/config and your .bash_login file."
  echo "If uninstalling, the autostart file at"\
       "~/.config/autostart/infotv-launch.desktop will no longer point at"\
       "anything so you may want to remove it."
}

_help () {
  echo "Usage:"
  echo "$0 enable"
  echo "$0 disable"
  echo "    Enable or disable the info-TV system."
  echo ""
  echo "$0 status"
  echo "    Show current status of the info-TV system."
  echo ""
  echo "$0 help"
  echo "    Show this help."
}

if [ "${1,,}" == "enable" ]; then
  _enable "$2"
elif [ "${1,,}" == "disable" ]; then
  _disable
elif [ "${1,,}" == "status" ]; then
  _status
else
  _help
fi
